package com.nikasov.skysofttech.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nikasov.skysofttech.R;
import com.nikasov.skysofttech.server.Item;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Item> items;

    public RecyclerAdapter(Context context, List<Item> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position).getItemType() == 1){
            return 0;
        }
        else
            return 1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case 0:
                return new ViewHolderFirst(LayoutInflater.from(context).inflate(R.layout.item_first_tab, parent, false));
            case 1:
                return new ViewHolderSecond(LayoutInflater.from(context).inflate(R.layout.item_second_tab, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Item item = items.get(position);
        switch (holder.getItemViewType()){
            case 0:
                Log.d("LOGN", getItemViewType(position) + "first type");
                ViewHolderFirst viewHolderFirst = (ViewHolderFirst) holder;
                Picasso.get().load(item.getUrl()).into(viewHolderFirst.image);
                viewHolderFirst.position.setText(String.valueOf(position));
                break;
            case 1:
                Log.d("LOGN", getItemViewType(position) + "sec type");
                ViewHolderSecond viewHolderSecond = (ViewHolderSecond) holder;
                Picasso.get().load(item.getUrl()).fit().centerCrop().into(viewHolderSecond.image);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolderFirst extends RecyclerView.ViewHolder  {

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.position)
        TextView position;

        ViewHolderFirst(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class ViewHolderSecond extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView image;

        ViewHolderSecond(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
