package com.nikasov.skysofttech;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class App extends Application {

    public static RealmConfiguration realmConfig;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(getApplicationContext());

        realmConfig = new RealmConfiguration.Builder()
                .name("myrealm.realm")
                .schemaVersion(7)
                .deleteRealmIfMigrationNeeded()
                .build();
    }
}
