package com.nikasov.skysofttech.server;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    private int itemType;
    @SerializedName("url")
    @Expose
    private String url;

    public Item(int itemType) {
        this.itemType = itemType;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }
}
