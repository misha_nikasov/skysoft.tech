package com.nikasov.skysofttech.server;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface API {
    @GET("/photos")
    Call<List<Item>> getAllItems();
}
