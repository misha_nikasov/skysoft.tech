package com.nikasov.skysofttech.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.nikasov.skysofttech.App;
import com.nikasov.skysofttech.data.Profile;

import io.realm.Realm;

@SuppressLint("Registered")
public class BasicActivity extends AppCompatActivity {

    public static final String TAG = "Nikasov";

    public static String HOME_LOGIN = "HOME";

    Realm realm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

        realm = Realm.getInstance(App.realmConfig);
    }

    public void showToast(Context context, String msg){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
