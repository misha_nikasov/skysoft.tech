package com.nikasov.skysofttech.activity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.login.widget.LoginButton;
import com.google.android.material.textfield.TextInputLayout;
import com.nikasov.skysofttech.App;
import com.nikasov.skysofttech.R;
import com.nikasov.skysofttech.data.Profile;

import java.util.UUID;

public class LoginActivity extends BasicActivity {

    @BindView(R.id.email_edit)
    TextInputLayout emailView;
    @BindView(R.id.pass_edit)
    TextInputLayout passView;

    @BindView(R.id.login_fb_button)
    LoginButton loginFbButton;
    @BindView(R.id.google_sign)
    Button googleBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        realm = Realm.getInstance(App.realmConfig);

        checkIfLogged();
    }

    private void checkIfLogged() {

        if (getIntent().getStringExtra(HOME_LOGIN) != null && getIntent().getStringExtra(HOME_LOGIN).equals("home")){
            return;
        }

        realm.beginTransaction();
        final RealmResults<Profile> profiles = realm.where(Profile.class).findAll();
        realm.commitTransaction();

        if (profiles.size() != 0){
            startProfile();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void startProfile() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.log_in)
    void logInWithUsername() {

        if (isValidateEmail() && isPasswordCorrect()){
            String email = emailView.getEditText().getText().toString().trim();
            String password = passView.getEditText().getText().toString().trim();

            saveProfile(password, email, email, "", "default", "default_token");
            startProfile();
        }else {
            return;
        }
    }

    private void saveProfile(String password, String email, String firstName, String secName, String url, String token){

        realm.executeTransaction(realm -> realm.where(Profile.class).findAll().deleteAllFromRealm());

        realm.executeTransaction(realm -> {
            Profile profile = realm.createObject(Profile.class, UUID.randomUUID().toString());
            profile.setEmail(email);
            profile.setPassword(password);
            profile.setFirstName(firstName);
            profile.setSecondName(secName);
            profile.setImageUrl(url);
            profile.setToken(token);
        });
    }

    private boolean isValidateEmail(){
        String email = emailView.getEditText().getText().toString().trim();

        if (email.isEmpty()){
            emailView.setError("Field can't be empty");
            return false;
        }
        else if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailView.setErrorEnabled(false);
            return true;
        }
        else {
            emailView.setError("Incorrect email");
            return false;
        }
    }

    private boolean isPasswordCorrect(){
        String pass = passView.getEditText().getText().toString().trim();

        if (pass.isEmpty()){
            passView.setError("Field can't be empty");
            return false;
        }
        else if (pass.length()<8){
            passView.setError("Password is too short");
            return false;
        }
        else {
            passView.setErrorEnabled(false);
            return true;
        }
    }
}
