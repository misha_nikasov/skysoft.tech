package com.nikasov.skysofttech.activity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.widget.Toolbar;

import com.nikasov.skysofttech.App;
import com.nikasov.skysofttech.R;
import com.nikasov.skysofttech.data.Profile;
import com.squareup.picasso.Picasso;

public class ProfileActivity extends BasicActivity {

    @BindView(R.id.profile_pic)
    ImageView profilePicView;
    @BindView(R.id.username_first)
    EditText usernameFirstView;
    @BindView(R.id.username_second)
    EditText usernameSecondView;

    private String usernameFirst, usernameSecond, profilePic;

    private Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ButterKnife.bind(this);
        realm = Realm.getInstance(App.realmConfig);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Profile");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getData();
    }

    private void getData() {
        realm.executeTransaction(bgRealm -> {
            profile = realm.where(Profile.class).findFirst();
            usernameFirst = profile.getFirstName();
            usernameSecond = profile.getSecondName();
            profilePic = profile.getImageUrl();
            setView();
        });
    }

    private void setView() {

        usernameFirstView.setText(usernameFirst);
        usernameSecondView.setText(usernameSecond);

        if (profilePic.equals("default")){
            profilePicView.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher_foreground));
        }
        else {
            Picasso.get().load(profilePic).into(profilePicView);
        }
    }

    @OnClick(R.id.save_btn)
    void save(){
        realm.executeTransaction(realm -> {
            profile.setFirstName(usernameFirstView.getText().toString());
            profile.setSecondName(usernameSecondView.getText().toString());
        });

        finish();
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
