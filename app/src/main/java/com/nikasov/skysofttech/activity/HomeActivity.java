package com.nikasov.skysofttech.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.nikasov.skysofttech.App;
import com.nikasov.skysofttech.R;
import com.nikasov.skysofttech.adapter.TabAdapter;
import com.nikasov.skysofttech.data.Profile;
import com.nikasov.skysofttech.fragment.FirstTabFragment;
import com.nikasov.skysofttech.fragment.SecondTabFragment;
import com.squareup.picasso.Picasso;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

public class HomeActivity extends BasicActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;

    private Toolbar toolbar;
    private Profile profile;

    private TabAdapter tabAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);
        realm = Realm.getInstance(App.realmConfig);

        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Home");

        tabAdapter = new TabAdapter(getSupportFragmentManager());

        setUpHeader();

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setUpHeader() {

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = headerView.findViewById(R.id.header_text);
        ImageView imageView = headerView.findViewById(R.id.imageView);

        realm.executeTransaction(realm -> {
            profile = realm.where(Profile.class).findFirst();
            navUsername.setText(profile.getFirstName() + " " + profile.getSecondName());

            if (profile.getImageUrl().equals("default")){
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher_foreground));
            }
            else {
                Picasso.get().load(profile.getImageUrl()).into(imageView);
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        TabAdapter tabAdapter = new TabAdapter(getSupportFragmentManager());
        tabAdapter.addFragment(new FirstTabFragment(), "First");
        tabAdapter.addFragment(new SecondTabFragment(), "Second");
        viewPager.setAdapter(tabAdapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_logout) {

            realm.executeTransaction(realm -> realm.where(Profile.class).findAll().deleteAllFromRealm());

            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);

            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        Intent intent = null;

        if (id == R.id.nav_profile) {
            intent = new Intent(this, ProfileActivity.class);
        } else if (id == R.id.nav_login) {
            intent = new Intent(this, LoginActivity.class);
            intent.putExtra(HOME_LOGIN, "home");
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        startActivity(intent);

        return true;
    }
}
