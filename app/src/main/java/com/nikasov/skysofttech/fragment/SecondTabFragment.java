package com.nikasov.skysofttech.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nikasov.skysofttech.R;
import com.nikasov.skysofttech.adapter.RecyclerAdapter;
import com.nikasov.skysofttech.server.Item;
import com.nikasov.skysofttech.server.NetworkService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SecondTabFragment extends Fragment {

    public SecondTabFragment() {}

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    private List<Item> items = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second_tab, container, false);

        ButterKnife.bind(this, view);

        getData();

        return view;
    }

    private void getData() {
        NetworkService.getInstance()
                .getJSONApi()
                .getAllItems()
                .enqueue(new Callback<List<Item>>() {
                    @Override
                    public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                        items = response.body();
                        List<Item> sortItems = new ArrayList<>();

                        for (int i = 0; i < 10; i++) {
                            sortItems.add(items.get(i));
                        }
                        setList(sortItems);
                    }

                    @Override
                    public void onFailure(Call<List<Item>> call, Throwable t) {
                        Log.d("Nikasov", t.getLocalizedMessage());
                    }
                });
    }

    private void setList(List<Item> sortItems) {
        RecyclerAdapter adapter = new RecyclerAdapter(getContext(), sortItems);
        RecyclerView.LayoutManager manager = new GridLayoutManager(getContext(), 4);

        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
    }
}
